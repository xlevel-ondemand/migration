var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
   db.changeColumn('pets', 'newColumn', {
      type: 'decimal', length: '10,2'
   }, callback);
};

exports.down = function(db, callback) {

};
